from manim import *
import math
import numpy as np

class BadMetaphor(Scene):
    def construct(self):
        self.wait(1.5)

        self.play(Write(Title('Things artifical networks ignore')), run_time=1)
        title = self.mobjects[-1]

        bullets = BulletedList(
            'Larger-scale chemical interactions',
            'The smaller number of dendritic connections',
            'Electrical activations being largely on/off',
            'Complex processes within each cell',
            'Irregular network topology',
            r'etc\dots')
        self.play(AnimationGroup(*map(Write, bullets), lag_ratio=0.5), run_time=4)
        self.wait(3)
        self.play(Unwrite(title), *map(Unwrite, bullets))

        equality = Tex(r'Machine Learning \\ $=$ \\ Statistics')
        self.play(Write(equality))
        self.wait(2)
        self.play(Unwrite(equality))

        time = SVGMobject('content/time.svg')
        self.play(Create(time))
        self.wait(3)
        self.play(Uncreate(time))

        title = Title('Training')
        self.play(Write(title))

        np.random.seed(45)

        N = 50
        tys = np.tile([0, 1], N//2)
        ts = np.random.uniform(-1, 1, N)
        xs = ts + np.random.normal(0, 0.05, N) + 0.2*tys - 0.1
        ys = ts + np.random.normal(0, 0.05, N) - 0.2*tys + 0.1

        axes = Axes(
            x_range=[-1.1, 1.1, 0.2],
            y_range=[-1.1, 1.1, 0.2],
            x_length=4,
            y_length=4,
            tips=False)
        axes.move_to([-3, 0, 0])

        dots = [Dot(point=axes.c2p(x, y), color=[RED, BLUE][ty]) for ty, x, y in zip(tys, xs, ys)]

        self.play(Create(axes), *map(Create, dots))
        self.wait(1)

        arrow = Arrow(start=LEFT, end=RIGHT)
        self.play(Write(arrow), run_time=0.5)

        realmtx = DecimalMatrix([
            [-1.12,  1.27],
            [ 0.63,  1.82],
            [-1.82, -0.35]],
            element_to_mobject_config={"num_decimal_places": 2})
        realmtx.move_to([3, 0, 0])
        self.play(Write(realmtx), run_time=0.5)

        self.wait(5)
        self.play(
            Uncreate(axes),
            *map(Uncreate, dots),
            Unwrite(title),
            Unwrite(arrow),
            Unwrite(realmtx),
            run_time=1)

        neuron = ImageMobject('content/neuron.png').set_width(2.5).shift(0.3 * DOWN)
        arrow1 = Arrow(start=LEFT*4, end=LEFT*1.5, stroke_width=10)
        arrow2 = Arrow(start=RIGHT*1.5, end=RIGHT*4, stroke_width=10)
        self.play(AnimationGroup(FadeIn(neuron), Write(arrow1), Write(arrow2), lag_ratio=0.5), run_time=2)
        self.wait(1)
        self.play(Wiggle(neuron, scale_value=1.2, rotation_angle=10*DEGREES, n_wiggles=5), run_time=2)
        self.wait(1)

        stats = Text('Statistics').scale(1.5)
        cross = Cross(stats)
        self.play(FadeOut(neuron), Unwrite(arrow1), Unwrite(arrow2), Write(stats), Write(cross), run_time=0.5)
        self.wait(3)
        self.play(Unwrite(stats), Unwrite(cross), run_time=0.5)

class Intro(Scene):
    def construct(self):
        robot = ImageMobject('content/public_domain_robot.png').set(height=6).rotate(-10*DEGREES)

        self.play(FadeIn(robot, run_time=1))
        self.wait(4)
        self.play(FadeOut(robot, run_time=1))

        self.wait(2)

        pets = Group(
            ImageMobject('content/cat.png').set(height=4),
            ImageMobject('content/dog.png').set(height=4))
        pets.arrange()
        cat, dog = pets
        split = Line(2*DOWN+0.6*LEFT, 2*UP+0.6*RIGHT, stroke_width=10)
        self.play(*map(FadeIn, pets), run_time=0.5)
        self.play(Write(split), ApplyMethod(cat.shift, 2*LEFT), ApplyMethod(dog.shift, 2*RIGHT), run_time=1)
        self.wait(2)
        self.play(FadeOut(cat), FadeOut(dog), Unwrite(split), run_time=0.5)

        route = ImageMobject('content/map.png').set(height=6)
        credit = Text('© OpenStreetMap contributors').scale(0.3).to_corner(DOWN+RIGHT)
        self.play(FadeIn(route), Write(credit), run_time=0.5)
        arc = Arc(radius=4.5, start_angle=0.9*PI, angle=0.8*PI, color=BLUE, stroke_width=10).move_to([1, 0.2, 0])
        self.play(Write(arc), run_time=1)
        self.wait(0.5)
        self.play(Unwrite(arc), run_time=1)
        self.play(FadeOut(route), Unwrite(credit), run_time=0.5)

        self.wait(4)

class Conclusion(Scene):
    def construct(self):
        words = Tex(r'Avocado\\Armchair')
        arrow = Arrow(start=LEFT, end=RIGHT)
        chair = ImageMobject('content/armchair.png').set_height(2)
        words.next_to(arrow, LEFT, buff=0.5)
        chair.next_to(arrow, RIGHT, buff=0.5)

        self.play(AnimationGroup(Write(words), Write(arrow), FadeIn(chair), lag_ratio=0.5), run_time=2)
        self.wait(1)
        self.play(Unwrite(words), Unwrite(arrow), FadeOut(chair), run_time=0.5)

        pets = Group(
            ImageMobject('content/cat.png').set(height=2),
            ImageMobject('content/dog.png').set(height=2))
        pets.arrange()
        cat, dog = pets
        split = Line(DOWN+0.3*LEFT, UP+0.3*RIGHT)
        self.play(*map(FadeIn, pets), run_time=0.5)
        self.play(Write(split), ApplyMethod(pets[0].shift, LEFT), ApplyMethod(pets[1].shift, RIGHT), run_time=1)
        self.wait(1.5)

        blue = ImageMobject('content/blue_berry.png').set(height=1.5).move_to(dog.get_center())
        red = ImageMobject('content/red_berry.png').set(height=2).move_to(cat.get_center())
        self.play(FadeTransform(dog, blue), FadeTransform(cat, red), run_time=0.5)

        self.wait(1.5)
        self.play(FadeOut(red), FadeOut(blue), Unwrite(split), run_time=0.5)
        self.wait(1)

        credit = Tex(r'Made by Sam Sartor\\for the 2021 Summer of Math Exposition').scale(0.6)
        self.play(Write(credit))
        self.wait(1)
        self.play(Unwrite(credit))
