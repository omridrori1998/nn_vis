from manim import *
import math
import numpy as np

images = ["content/red_berry.png", "content/blue_berry.png"]

class ScatterBerries(Scene):
    def construct(self):
        red, blue = (ImageMobject(i).set(width=2) for i in images)
        blue.scale(0.6)
        g = Group(blue, red)
        g.arrange(buff = 2)
        red.shift(RIGHT)

        self.play(FadeIn(red), run_time=1)
        self.wait(0.5)
        self.play(FadeIn(blue), run_time=1)

        self.wait(2)

        camera = ImageMobject("content/camera.png").set(height=1.5)
        camera.next_to(g, UP)
        self.play(FadeIn(camera), run_time=1.5)
        self.play(Flash(camera, flash_radius=1), run_time=1)
        self.play(FadeOut(camera), run_time=1.5)

        self.wait(0.5)

        line = Line(start=[-4, 0, 0], end=[4, 0, 0])
        line.next_to(g, DOWN)
        tri = Triangle(color=WHITE)
        tri.next_to(line, DOWN)
        self.play(Create(line), Create(tri), run_time=1)
        self.wait(1)

        self.play(
            Rotate(line, angle=-30*DEGREES),
            ApplyMethod(red.shift, DOWN),
            ApplyMethod(blue.shift, UP),
            run_time=1)

        self.play(
            Rotate(line, angle=30*DEGREES),
            ApplyMethod(red.shift, UP),
            ApplyMethod(blue.shift, DOWN),
            run_time=1)

        axis1d = NumberLine(
            x_range=[1, 4, 0.5],
            length=10,
            include_numbers=False)

        decis = Dot(point=axis1d.number_to_point(2.4), radius=0.2)

        np.random.seed(43)

        N = 30
        tys = np.tile([0, 1], N//2)
        ts = np.random.uniform(0, 2, N)
        rs = 3 + ts + np.random.normal(0, 0.25, N)
        dws = 3 + np.random.normal(0, 0.25, N) - 1.0*tys
        ws = dws + 0.8 * ts

        dots = [
            ImageMobject(images[ty])
                .set(width=0.3)
                .move_to(axis1d.number_to_point(dw))
                .shift(0.5 * UP)
                .scale(0.6 if ty else 1)
            for ty, dw in zip(tys, dws)
        ]

        self.play(
            ReplacementTransform(line, axis1d),
            Uncreate(tri),
            *(FadeIn(d) for d in dots[2:]),
            ReplacementTransform(red, dots[0]),
            ReplacementTransform(blue, dots[1]),
            run_time=1.5)
        self.wait(2.5)

        self.play(Create(decis), run_time=1)
        self.wait(1)
        self.play(*(Wiggle(d, scale_value=1.5, n_wiggles=4, rotation_angle=20*DEGREES)
            for d, t in zip(dots, tys) if t == 0), run_time=3)
        self.play(*(Wiggle(d, scale_value=2, n_wiggles=4, rotation_angle=20*DEGREES)
            for d, t in zip(dots, tys) if t == 1), run_time=3)
        self.wait(4)
        self.play(Flash(decis), run_time=1)

        self.wait(4)
        self.play(*(ApplyMethod(d.shift, RIGHT) for d in dots), run_time=3)

        axes2d = Axes(x_range=[2.5, 6, 0.5], y_range=[1.5, 5, 0.5])

        self.wait(2.5)
        self.play(
            ReplacementTransform(axis1d, axes2d),
            Uncreate(decis),
            *(ApplyMethod(d.move_to, axes2d.coords_to_point(r, w)) for d, w, r in zip(dots, ws, rs)),
            run_time=2.5)
        self.wait(1)

        trendline = Line(
            axes2d.coords_to_point(3, 2.3),
            axes2d.coords_to_point(5.5, 4.3))

        self.play(Create(trendline), run_time=0.5)
        self.wait(1.5)

        self.play(
            Uncreate(axes2d),
            *(FadeOut(d) for d in dots),
            run_time=0.5)

class SimpleXY(Scene):
    def __init__(self):
        Scene.__init__(self)

        self.axes = Axes(
            x_range=[-1.2, 1.2, 0.5],
            y_range=[-1.2, 1.2, 0.5],
            x_length=6,
            y_length=6)
        np.random.seed(48)

        self.N = 40
        self.ts = np.random.uniform(-1, 1, self.N)
        self.xs = self.ts + np.random.normal(0, 0.1, self.N)
        self.ys = self.ts + np.random.normal(0, 0.1, self.N)

        self.dots = [Dot() for i in range(self.N)]
        self.line = Line()

        self.blue_dots = self.dots[:self.N//2]
        self.red_dots = self.dots[self.N//2:]

        for d in self.blue_dots:
            d.set_color(BLUE)

        for d in self.red_dots:
            d.set_color(RED)

        self.set_points()

    def set_points(self):
        for x, y, d in zip(self.xs, self.ys, self.dots):
            d.move_to(self.axes.coords_to_point(x, y))

        self.line.set_points_by_ends(
            self.axes.coords_to_point(-1, -1),
            self.axes.coords_to_point(1, 1))

class LogisticOrLinear(SimpleXY):
    def construct(self):
        self.axes.to_edge(LEFT)
        self.set_points()

        self.wait(0.5)
        self.play(
            Create(self.axes),
            *map(Create, self.blue_dots),
            Create(self.line),
            run_time=0.5)

        title1 = Tex(r'\underline{Linear Regression}').to_corner(UP+RIGHT).shift(LEFT+DOWN)
        title2 = Tex(r'\underline{Logistic Regression}').to_corner(UP+RIGHT).shift(LEFT+DOWN)
        title4 = Tex(r'\underline{Machine Learning}').to_corner(UP+RIGHT).shift(LEFT+DOWN)

        eqs = [
            r'ax + b = y',
            r'a{{x}} + b = {{y}}',
            r'\beta_1 {{x}} + \beta_0 = {{y}}',
            r'{{\beta_1 x}} + {{\beta_0}} = {{y}}',
            r'{{\beta_1 x}} + \beta_2 {{y}} + {{\beta_0}} = 0',
            r'{{\beta_1 x + \beta_2 y + \beta_0}} {{=}} 0',
            r'\sigma({{\beta_1 x + \beta_2 y + \beta_0}}) {{=}} \bullet / \bullet',
        ]
        eqs = [MathTex(e).next_to(title1, DOWN).shift(DOWN) for e in eqs]

        title3 = title2.copy()
        title3_target = title1.copy().next_to(eqs[-1], DOWN)
        eq3 = eqs[-1].copy()
        eq3_target = eqs[5].copy().next_to(title3_target, DOWN)

        eqs[6][4][0].set_color(RED)
        eqs[6][4][2].set_color(BLUE)

        anims = [[], [], [
            ReplacementTransform(title1, title2),
            *(ApplyMethod(d.shift, [0.5, -0.5, 0]) for d in self.blue_dots),
            *(Create(d.shift([-0.5, 0.5, 0])) for d in self.red_dots),
        ]]

        self.play(Write(title1), Write(eqs[0]), run_time=1)
        self.wait(1.5)

        for i, (a, b) in enumerate(zip(eqs[0:], eqs[1:])):
            if i % 2 == 0:
                self.remove(a)
                self.add(b)
            else:
                self.play(TransformMatchingTex(a, b), *anims[i // 2], run_time=2)
        self.wait(3)

        self.add(title3, eq3)
        self.play(
            ApplyMethod(eqs[-1].next_to, title2, DOWN),
            Transform(title3, title3_target),
            TransformMatchingTex(eq3, eq3_target),
            run_time=2)
        self.wait(5)

        dotdotdot = Tex(r'\dots')\
            .next_to(eqs[-1], DOWN)\
            .rotate(90*DEGREES)\
            .shift(DOWN)

        self.play(
            Transform(title2, title4),
            Unwrite(title3),
            ApplyMethod(eq3_target.next_to, eqs[-1], DOWN),
            Write(dotdotdot),
            run_time=2)
        self.wait(3)

        self.play(
            Uncreate(self.axes),
            Uncreate(self.line),
            Unwrite(title2),
            Unwrite(eqs[-1]),
            Unwrite(eq3_target),
            Unwrite(dotdotdot),
            *(Uncreate(d) for d in self.dots),
            run_time=0.5)

class LineOrTransform(SimpleXY):
    def construct(self):
        self.axes
        for d in self.blue_dots:
            d.shift([0.5, -0.5, 0])

        for d in self.red_dots:
            d.shift([-0.5, 0.5, 0])

        self.play(
            Create(self.axes),
            *map(Create, self.dots),
            Create(self.line),
            run_time=0.5)

        self.wait(3.5)
        self.play(Indicate(self.line))
        self.wait(0.5)

        self.play(Rotate(self.line, angle=-45*DEGREES), run_time=2)
        self.play(Rotate(self.line, angle=45*DEGREES), run_time=1.5)

        self.play(
            Succession(
                Transform(self.line, Line(ORIGIN+LEFT, ORIGIN+RIGHT), run_time=3.5),
                Uncreate(self.line, run_time=0.5)),
            *(Rotate(d, angle=-45*DEGREES, about_point=ORIGIN) for d in self.dots),
            run_time=4)

        self.wait(6)

class SaturatedBerries(Scene):
    def construct(self):
        np.random.seed(45)

        N = 50
        tys = np.tile([0, 1], N//2)
        ts = np.random.uniform(0, 1, N)
        rs = ts + np.random.normal(0, 0.05, N)
        off = np.tile([0.1, -0.1], N//2)
        ws = np.tanh(3*ts + off) + off + np.random.normal(0, 0.05, N)

        axes = Axes(x_range=[0, 1.2, 0.2], y_range=[0, 1.2, 0.2])


        dots = [
            ImageMobject(images[ty])
                .set(width=0.2)
                .move_to(axes.coords_to_point(r+0.1, w+0.1))
                .scale(0.8 if ty else 1)
            for ty, r, w in zip(tys, rs, ws)
        ]

        self.wait(1)
        self.play(Create(axes), *map(FadeIn, dots))
        (xaxis, yaxis) = axes.get_axes()

        self.wait(7)

        line = Line(
            axes.coords_to_point(0, 0),
            axes.coords_to_point(1.1, 1.1))
        red_line = line.copy().set_color(RED)

        self.play(Create(red_line), run_time=1)
        self.wait(1)
        self.play(ApplyWave(red_line, ripples=2), run_time=2)
        self.play(Uncreate(red_line), run_time=1)

        self.wait(1)

        dur = 3
        time = 0
        def tanslide(axes, dt):
            nonlocal dur, rs, ws, dots, time

            time += dt
            t = rate_functions.smooth(time/dur)

            for d, r, w in zip(dots, rs, ws):
                a = r
                b = math.tanh(3*r)
                d.move_to(axes.coords_to_point(t*b + (1-t)*a + 0.1, w+0.1))

            for x, tick in zip(np.arange(0.2, 1.2, 0.2), xaxis.get_tick_marks()[0]):
                a = x-0.1
                b = math.tanh(3*a)
                x0, y0, z0 = tick.get_center()
                tick.move_to(axes.coords_to_point(t*b + (1-t)*a + 0.1, 0))

        axes.add_updater(tanslide)
        self.wait(3)
        axes.remove_updater(tanslide)

        line.set_color(WHITE)
        self.play(Create(line), run_time=1)

        dur = 3
        time = 0
        def linslide(axes, dt):
            nonlocal dur, rs, ws, dots, time

            time += dt
            t = rate_functions.smooth(time/dur)

            for d, r, w in zip(dots, rs, ws):
                x = math.tanh(3*r)
                d.move_to(axes.coords_to_point(x + 0.1, w - t*(x-0.5) + 0.1))

        pivot = yaxis.number_to_point(0.6)
        axis_target = xaxis.get_center()
        axis_target[1] = pivot[1]

        axes.add_updater(linslide)
        self.play(
            Transform(line, xaxis.copy().move_to(axis_target)),
            Uncreate(xaxis),
            run_time=dur)
        axes.remove_updater(linslide)

        self.wait(3)

        self.play(Uncreate(axes), Uncreate(line), *map(FadeOut, dots))
